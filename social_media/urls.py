from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'social_media.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    (r'^$', 'sm.views.index', { }, 'root'),
    (r'^login/$', 'login', { }, 'login'),
	(r'^logout/$', 'logout', { }, 'logout'),
    (r'^auth/$', include('sm.urls.auth_urls', namespace="auth")),

    #(r'^EXAMPLE/$', include('sm.urls.SOME_URLS')),

)
