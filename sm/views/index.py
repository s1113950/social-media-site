from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone

from sm.decorators import protected_view

from sm.forms import LoginForm

@protected_view
def index(request):
	form = LoginForm()

	context = {
		'form': form,
	}

	return render(request, 'templates/index.html', context)

