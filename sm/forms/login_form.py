from django import forms

from sm.models import User

class LoginForm(forms.ModelForm):

	class Meta:
		model = User