from django.contrib import admin
from django.conf.urls import patterns, include, url
admin.autodiscover()

urlpatterns = patterns('',
	(r'^$', 'sm.views.index', { }, 'root'), 
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls)),
    (r'^login/', 'uws_app.views.authentication.login', { }, 'basic_login'),

)


