from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class User(models.Model):

	first_name = models.CharField(max_length=128)
	last_name = models.CharField(max_length=128)
	email = models.EmailField()
	phone = PhoneNumberField(default=None)
	password = models.CharField(max_length=128)

	class Meta:
		unique_together = ['first_name', 'last_name', 'email', 'phone', 'password']
		app_label = 'sm'