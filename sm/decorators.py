import time
import traceback

from django.shortcuts import render
from django.contrib import messages

def protected_view(view):
    def inner(*args, **kwargs):
        from django.http import HttpResponse
        try:
            func = view(*args, **kwargs)
            return func
        except Exception as e:
            traceback.print_exc()
    return inner

def timer(view):
    def inner(*args, **kwargs):
        begin = time.clock()
        func = view(*args, **kwargs)
        print "{0} runtime: {1}sec".format(view.func_name, time.clock() - begin)
        return func

    return inner
